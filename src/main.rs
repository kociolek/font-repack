//Our modules
mod keys;
use keys::KNOWN_KEYS;

mod fonts;
use fonts::FontFileDescription;
//External modules
extern crate crypto;
extern crate getopts;
extern crate byteorder;

use crypto::blowfish::Blowfish;
use crypto::symmetriccipher::BlockEncryptor;
use crypto::symmetriccipher::BlockDecryptor;
use std::cmp::min;
use std::io::prelude::*;
use std::fs::File;
use std::env;
use std::path::Path;
use getopts::Options;
use std::process;

struct Configuration {
    pub game_id : usize,
    pub target_file_name : String,
    pub output_file_name : String
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

fn get_options() -> Option<Configuration> {
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();
    opts.optflag("h", "help",   "print this help menu");
    opts.optflag("",  "games",  "print supported games names and version with brackets");
    opts.optopt("g",  "game",   "set game keyfile",     "NUMBER");
    opts.optopt("i",  "input",  "set target file",      "FILE.font");
    opts.optopt("o",  "output", "set output file name", "FILE.font");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => {m}
        Err(f) => {
            println!("{}", f.to_string());
            return None;
        }
    };

    if matches.opt_present("games") {
        print_known_keys();
        return None;
    }

    if matches.opt_present("h") {
        print_usage(&args[0].clone(), opts);
        return None;
    }
    //So cool
    Some(Configuration {
        //0 -- auto guess mode
        game_id: matches.opt_str("g").and_then(|m| m.parse::<usize>().ok()).unwrap_or(0),

        target_file_name: match matches.opt_str("i") {
            Some(m) => m,
            None => {
                println!("Required option 'i' missing.");
                print_usage(&args[0].clone(), opts);
                return None;
            }
        },

        output_file_name: match matches.opt_str("o") {
            Some(m) => m,
            None => {
                println!("Required option 'o' missing.");
                print_usage(&args[0].clone(), opts);
                return None;
            }
        }
    })
}

fn main() {
    let config = match get_options() {
        Some(e) => e,
        _ => process::exit(1)
    };

    let font_archive = env::current_exe()
        .ok().map(|p|p.clone()).as_ref()
        .and_then(|p|p.parent().map(|parent|parent.to_path_buf()))
        .or_else(||env::current_dir().ok()) //Default path just in case
        .map(|p|p.join(Path::new("fonts.tar")))
        .as_ref().and_then(|p|p.to_str())
        .map(|x|fonts::FontArchive::open(x))
        .expect("Opening fonts archive failed!");

    let mut font_file = Path::new(&config.target_file_name).file_name()
            .and_then(|p| p.to_str())
            .and_then(|file_name| font_archive.get_font_file(&file_name))
            .expect("Incorrect font file name");

    let mut dds_file = font_file.get_dds()
        .expect("Error while reading archive file!");
    let font_table_file = font_file.get_hdr()
        .expect("Error while reading archive file!");

    let mut target_font_file = Vec::<u8>::new();
    File::open(&config.target_file_name)
        .and_then(|mut f| f.read_to_end(&mut target_font_file))
        .ok().expect("target font -> IO Error");

    let mut encoded_dds_file = File::create(&config.output_file_name).ok()
        .expect("output");

    println!("Reading target file content...");

    //Read file description
    let font_file_desc = fonts::read_file(&target_font_file)
        .expect("Font file corrupted");

    let key_id = if config.game_id == 0 {
        println!("Guessing game key. This might be inaccurate!");
        match guess_game_id(&target_font_file, &font_file_desc) {
            Some(key_id) => key_id,
            None => panic!("Unable to guess game key!")
        }
    } else if config.game_id > KNOWN_KEYS.len() {
        panic!("Incorrect game number");
    } else {
       config.game_id - 1
    };

    println!("Using key for game: {}", KNOWN_KEYS[key_id].name);
    println!("Rewriting font file...");
	encrypt(&mut dds_file, &KNOWN_KEYS[key_id].key);
    //Header (from target file)
    encoded_dds_file.write_all(&target_font_file[0..font_file_desc.header_end])
        .ok().expect("Font saving error");
    //Font description (from table file)
    encoded_dds_file.write_all(&font_table_file).ok()
        .expect("Font saving error");
    //header end ... the place
    encoded_dds_file.write_all(&target_font_file[font_file_desc.file_end..font_file_desc.the_place]).ok().expect("Font saving error");
    //DDS
    encoded_dds_file.write_all(&dds_file).ok().expect("Font saving error");
    println!("Done!");
}

//test function
#[test]
fn test_encode() {
    let mut plain : Vec<u8> = vec![0x44u8, 0x44u8, 0x53u8, 0x20u8, 0x7cu8, 0x00u8, 0x00u8, 0x00u8];
    let key = KNOWN_KEYS[1].key;
    encrypt(&mut plain, &key);
    assert!(plain == vec![0xed, 0x9e, 0xaa, 0xbf, 0x92, 0xa8, 0x72, 0xe8]);
}

fn guess_game_id(file : &[u8], file_desc : &FontFileDescription) -> Option<usize> {
    //Read 8 bytes from file
    let mut decoded = [0u8; 8];
    if file.len() < 8 {
        return None;
    }

    let mut buffer = [0u8; 8];
    for i in 0..8 {
        buffer[i] = file[file_desc.the_place + i];
    }

    reverse_blocks(&mut buffer);
    for (idx, key_desc) in KNOWN_KEYS.iter().enumerate() {
        let state = Blowfish::new(&key_desc.key);
        state.decrypt_block(&buffer, &mut decoded);
        //"DDS " --(reversed)--> " SDD"
        if decoded[0] == b' ' &&
           decoded[1] == b'S' &&
           decoded[2] == b'D' &&
           decoded[3] == b'D' {
               return Some(idx);
           }
    }
    return None;
}

fn print_known_keys() {
    println!("Known game keys: ");
    for (idx, key) in KNOWN_KEYS.iter().enumerate() {
        println!(" [{}]: {}", idx + 1, key.name);
    }
}

//Odwraca kolejnosc bajtow w bloku.
//Konieczne do szyfrowania, z jakiegos powodu tego oczekuje
//gra podczas dekodowania
fn reverse_blocks(data: &mut [u8]) {
    for chunk in data.chunks_mut(4) {
        if chunk.len() == 4 {
            chunk.reverse();
        }
    }
}

//Srednio ladna metoda kodowania... ale dziala
fn encrypt(data : &mut Vec<u8>, key : &[u8]) {
	let state = Blowfish::new(&key);
    let encrypted_chunk_size = min(0x800, data.len());
    let mut encrypted_chunk = &mut data[0..encrypted_chunk_size];

    //Holds intermediate result (BlockSize = 8)
	let mut output = [0u8; 8];

    reverse_blocks(encrypted_chunk);

	for chunk in encrypted_chunk.chunks_mut(8) {
        //Encode ONLY full blocks
		if chunk.len() == 8 {
			state.encrypt_block(&chunk, &mut output);
			for idx in 0..8 {
				chunk[idx] = output[idx];
			}
		}
	}
    reverse_blocks(encrypted_chunk);
}
