extern crate tar;

use std::io::prelude::*;
use std::fs::File;
use self::tar::Archive;

pub struct FontFileDescription {
    pub file_end : usize,
    pub header_end : usize,
	pub the_place : usize
}

pub struct FontArchive {
    archive : Archive<File>
}

pub struct FontFile<'a> {
    hdr_file : tar::File<'a, File>,
    dds_file : tar::File<'a, File>
}

impl FontArchive {
    pub fn open(file : &str) -> FontArchive {
        let file = File::open(file).unwrap();
        FontArchive { archive: Archive::new(file) }
    }

    pub fn get_font_file(&self, font : &str) -> Option<FontFile> {
        let mut hdr_file : Option<tar::File<File>> = None;
        let mut dds_file : Option<tar::File<File>> = None;
        let dds_file_name = font.to_string() + ".dds";
        let hdr_file_name = font.to_string() + ".hdr";

        for a in self.archive.files().unwrap() {
            let file = a.unwrap();
            if file.filename().unwrap() == dds_file_name {
                dds_file = Some(file);
            } else if file.filename().unwrap() == hdr_file_name {
                hdr_file = Some(file);
            }
            if hdr_file.is_some() && dds_file.is_some() {
                break;
            }
        }
        if hdr_file.is_none() || dds_file.is_none() {
            None
        } else {
            Some(FontFile::new(hdr_file.unwrap(), dds_file.unwrap()))
        }
    }
}

impl <'a> FontFile<'a> {
    fn new (hdr_file : tar::File<'a, File>,
            dds_file : tar::File<'a, File>) -> FontFile<'a> {
        FontFile { hdr_file: hdr_file, dds_file: dds_file }
    }

    fn read_file_as_vec(file : &mut tar::File<File>) -> Option<Vec<u8>> {
        let mut file_content = Vec::<u8>::new();
        match file.read_to_end(&mut file_content) {
            Ok(_) => Some(file_content),
            _ => None
        }
    }

    pub fn get_dds(&mut self) -> Option<Vec<u8>> {
        FontFile::read_file_as_vec(&mut self.dds_file)
    }

    pub fn get_hdr(&mut self) -> Option<Vec<u8>> {
        FontFile::read_file_as_vec(&mut self.hdr_file)
    }
}

pub fn read_file(buff: &Vec<u8>) -> Option<FontFileDescription> {
	let file_size = buff.len();
	let mut header_end = file_size / 2;
	let mut file_end = file_size - 100;
	for i in 1..file_size / 2 {
		if buff[i] == 0
            && buff[i+1] == 0
            && buff[i+2] == 0
            && buff[i+3] == 0 {

                header_end = i;
			    break;
		}
    }

	if header_end == file_size / 2 {
		println!("Incorrect file format: Header not found");
        return None;
	}
	//Magic but works...
	header_end += 2;

	// szukanie konca pliku
	for i in header_end..file_size - 100 {
		if buff[i] == b'D'
            && buff[i+1] == b'X'
            && buff[i+2] == b'T'
            && buff[i+3] == b'5' {

			file_end = i;
			break;
		}
	}

	if file_end == file_size - 100 {
		println!("Incorrect file format: Image header corrupted");
        return None;
	}
	// The Place Where Everything Ends And Begins
	let mut the_place = file_size/2;
	for i in file_end..file_end + 100 {
        let current_value = (buff[i] as usize)
            + (buff[i + 1] as usize)*256
            + (buff[i + 2] as usize)*256*256
            + (buff[i + 3] as usize)*256*256*256;
		if current_value == file_size - i - 4 {
			the_place = i;
			break;
		}
	}
	if the_place == file_end + 100 {
		println!("Incorrect file format: Unable to determine image size");
        return None;
	}
	the_place += 4;

	Some(FontFileDescription {
        file_end: file_end,
        header_end: header_end,
        the_place: the_place
    })
}
